resource "aci_tenant" "terraform_ten" {
  name = var.tenant_name
}

resource "aci_vrf" "vrf1" {
  tenant_dn = aci_tenant.terraform_ten.id
  name      = var.vrf
}

resource "aci_cloud_aws_provider" "cloud_apic_provider" {
  tenant_dn         = aci_tenant.terraform_ten.id
  access_key_id     = var.aws_access_key
  secret_access_key = var.aws_secret_key
  account_id        = var.account_id
  is_trusted        = "no"
}

resource "aci_bridge_domain" "bd1" {
  tenant_dn          = aci_tenant.terraform_ten.id
  relation_fv_rs_ctx = aci_vrf.vrf1.name
  name               = var.bd
}

resource "aci_cloud_context_profile" "context_profile" {
  name                     = "devnet-admin-cloud-ctx-profile"
  description              = "context provider created with terraform"
  tenant_dn                = aci_tenant.terraform_ten.id
  primary_cidr             = var.cidr
  region                   = var.aws_region
  relation_cloud_rs_to_ctx = aci_vrf.vrf1.name
}

data "aci_cloud_cidr_pool" "prim_cidr" {
  cloud_context_profile_dn = aci_cloud_context_profile.context_profile.id
  addr                     = var.cidr
}


resource "aci_cloud_subnet" "cloud_apic_subnet" {
  cloud_cidr_pool_dn            = data.aci_cloud_cidr_pool.prim_cidr.id
  ip                            = var.subnet
  relation_cloud_rs_zone_attach = "uni/clouddomp/provp-aws/region-${var.aws_region}/zone-${var.cloud_zone}"
}

